#!/bin/bash

TRANSIT_FEED_API='https://api.transitfeeds.com/v1'

TRANSIT_FEED_LOCATION=${1:-'saskatoon'}
TRANSIT_FEED_KEY="$(if [ -z "$2" ]; then read -rsp 'Enter transit feeds api key: ' key; else key="$2"; fi; echo -n $key)"

location_id="$(curl -H 'Accept: application/json' "$TRANSIT_FEED_API/getLocations?key=${TRANSIT_FEED_KEY}" --silent |\
    jq ".results.locations[] | select((.n | ascii_downcase) == (\"$TRANSIT_FEED_LOCATION\" | ascii_downcase)) | .id?"\
)"

location_gtfs_zip="$(curl -H 'Accept: application/json' "$TRANSIT_FEED_API/getFeeds?key=${TRANSIT_FEED_KEY}&location=$location_id&type=gtfs" --silent |\
    jq --raw-output '.results.feeds[0]?.u.d'\
)"

mkdir -p bundle
curl --show-error "$location_gtfs_zip" --output bundle/gtfs.zip
